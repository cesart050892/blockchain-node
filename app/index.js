const express = require("express");
const Blockchain = require("../blockchain");

const P2PServer = require("../p2p");
const HTTP_PORT = process.env.HTTP_PORT || 3000;
const bodyParser = require("body-parser");
const server = express();

const bc = new Blockchain();
const p2p = new P2PServer(bc);

server.use(bodyParser.json());

server.get("/blocks", (req, res) => {
  res.json(bc.chain);
});

server.post("/mine", (req, res) => {
  const block = bc.addBlock(req.body.data);
  console.log(`New block added: ${block.toString()}`);
  p2p.sendChain();
  res.redirect("/blocks");
});

server.listen(HTTP_PORT, () => {
  console.log(`Http server listening port ${HTTP_PORT}`);
});

p2p.listen();
