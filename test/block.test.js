let Block = require("../blockchain/block");

describe("block", () => {
  let data, lastBlock, block;
  beforeEach(() => {
    data = "lorem";
    lastBlock = Block.genesis();
    block = Block.mineBlock(lastBlock, data);
  });

  it("Set the data match the input", () => {
    expect(block.data).toEqual(data);
  });

  it("Set the lastHash match the hash of the last block", () => {
    expect(block.lastHash).toEqual(lastBlock.hash);
  });
});
