let Blockchain = require("../blockchain/index");
let Block = require("../blockchain/block");

describe("blockchain", () => {
  let bc;
  let bc2;
  beforeEach(() => {
    bc = new Blockchain();
    bc2 = new Blockchain();
  });

  it("Start with the genesis block", () => {
    let genesis = bc.chain[0];
    expect(genesis).toEqual(Block.genesis());
  });

  it("Add new block", () => {
    bc.addBlock("lorem");
    expect(bc.chain[bc.chain.length - 1].data).toEqual("lorem");
  });

  it("Validate a valid chain", () => {
    bc2.addBlock("ipsum");
    expect(bc2.isValidChain(bc2.chain)).toBe(true);
  });

  it("Validate a chain with a corrupt genesis block", () => {
    bc2.chain[0].data = "jhskjhaskjsah";
    expect(bc.isValidChain(bc2.chain)).toBe(false);
  });

  it("Invalidate corrupt chain", () => {
    bc2.addBlock("foobar");
    bc2.chain[1].data = "uiaysdsadkjh";
    expect(bc.isValidChain(bc2.chain)).toBe(false);
  });

  it("Replace chain with a valid chain", () => {
    bc2.addBlock("google");
    bc.replaceChain(bc2.chain);
    expect(bc.chain).toEqual(bc2.chain);
  });

  it("Doesn't replace chain with one of less than or equal to length", () => {
    bc.addBlock("facebook");
    bc.replaceChain(bc2.chain);
    expect(bc.chain).not.toEqual(bc2.chain);
  });
});
